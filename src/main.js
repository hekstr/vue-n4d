import Vue from 'vue'
import VueResource from 'vue-resource'
// import VueRouter from 'vue-router'
import VueMoment from 'vue-moment'
import VueValidator from 'vue-validator'
Vue.use(VueResource)
Vue.use(VueMoment)
Vue.use(VueValidator)
Vue.http.headers.common['site_id'] = window.n4dSite.id
Vue.config.warnExpressionErrors = false

import App from './App'
import router from './router'
router.start(App, '#app')
import N4dfunctions from './functions'
N4dfunctions()

