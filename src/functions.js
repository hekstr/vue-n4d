import Waves from './functions/waves.js'
import SkinChange from './functions/skin-change.js'
import DetectMobileBrowsers from './functions/detect-mobile-browsers.js'
import ScrollTop from './functions/scroll-top.js'
// import Sticky from './functions/sticky.js'
module.exports = function() {
  ;(function() {
    Waves()
    SkinChange()
    DetectMobileBrowsers()
    ScrollTop()
    // Sticky()
  })()
}
