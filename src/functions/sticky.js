var $ = require('jquery')
module.exports = function() {
  (function() {
    var win = $(window)

    $.fn.stickInParent = function(opts) {
      var doc, elm, enableBottoming, fn, i, innerScrolling, len, manualSpacer, offsetTop, parentSelector, recalcEvery, stickyClass
      if (opts == null) {
        opts = {}
      }
      stickyClass = opts.stickyClass
      innerScrolling = opts.innerScrolling
      recalcEvery = opts.recalcEvery
      parentSelector = opts.parent
      offsetTop = opts.offsetTop
      manualSpacer = opts.spacer
      enableBottoming = opts.bottoming
      if (offsetTop == null) {
        offsetTop = 0
      }
      if (parentSelector == null) {
        parentSelector = void 0
      }
      if (innerScrolling == null) {
        innerScrolling = true
      }
      if (stickyClass == null) {
        stickyClass = 'is_stuck'
      }
      doc = $(document)
      if (enableBottoming == null) {
        enableBottoming = true
      }
      fn = function(elm, paddingBottom, parentTop, parentHeight, top, height, elFloat, detached) {
        var bottomed, detach, fixed, lastPos, lastScrollHeight, offset, parent, recalc, recalsAndTick, recalcCounter, spacer, tick
        if (elm.data('sticky_kit')) {
          return
        }
        elm.data('sticky_kit', true)
        lastScrollHeight = doc.height()
        parent = elm.parent()
        if (parentSelector != null) {
          parent = parent.closest(parentSelector)
        }
        if (!parent.length) {}
        fixed = false
        bottomed = false
        spacer = manualSpacer != null ? manualSpacer && elm.closest(manualSpacer) : $('<div />')
        if (spacer) {
          spacer.css('position', elm.css('position'))
        }
        recalc = function() {
          var borderTop, paddingTop, restore
          if (detached) {
            return
          }
          lastScrollHeight = doc.height()
          borderTop = parseInt(parent.css('border-top-width'), 10)
          paddingTop = parseInt(parent.css('padding-top'), 10)
          paddingBottom = parseInt(parent.css('padding-bottom'), 10)
          parentTop = parent.offset().top + borderTop + paddingTop
          parentHeight = parent.height()
          if (fixed) {
            fixed = false
            bottomed = false
            if (manualSpacer == null) {
              elm.insertAfter(spacer)
              spacer.detach()
            }
            elm.css({
              position: '',
              top: '',
              width: '',
              bottom: ''
            }).removeClass(stickyClass)
            restore = true
          }
          top = elm.offset().top - (parseInt(elm.css('margin-top'), 10) || 0) - offsetTop
          height = elm.outerHeight(true)
          elFloat = elm.css('float')
          if (spacer) {
            spacer.css({
              width: elm.outerWidth(true),
              height: height,
              display: elm.css('display'),
              'vertical-align': elm.css('vertical-align'),
              'float': elFloat
            })
          }
          if (restore) {
            return tick()
          }
        }
        recalc()
        if (height === parentHeight) {
          return
        }
        lastPos = void 0
        offset = offsetTop
        recalcCounter = recalcEvery
        tick = function() {
          var css, delta, recalced, scroll, willBottom, winHeight
          if (detached) {
            return
          }
          recalced = false
          if (recalcCounter != null) {
            recalcCounter -= 1
            if (recalcCounter <= 0) {
              recalcCounter = recalcEvery
              recalc()
              recalced = true
            }
          }
          if (!recalced && doc.height() !== lastScrollHeight) {
            recalc()
            recalced = true
          }
          scroll = win.scrollTop()
          if (lastPos != null) {
            delta = scroll - lastPos
          }
          lastPos = scroll
          if (fixed) {
            if (enableBottoming) {
              willBottom = scroll + height + offset > parentHeight + parentTop
              if (bottomed && !willBottom) {
                bottomed = false
                elm.css({
                  position: 'fixed',
                  bottom: '',
                  top: offset
                }).trigger('sticky_kit:unbottom')
              }
            }
            if (scroll < top) {
              fixed = false
              offset = offsetTop
              if (manualSpacer == null) {
                if (elFloat === 'left' || elFloat === 'right') {
                  elm.insertAfter(spacer)
                }
                spacer.detach()
              }
              css = {
                position: '',
                width: '',
                top: ''
              }
              elm.css(css).removeClass(stickyClass).trigger('sticky_kit:unstick')
            }
            if (innerScrolling) {
              winHeight = win.height()
              if (height + offsetTop > winHeight) {
                if (!bottomed) {
                  offset -= delta
                  offset = Math.max(winHeight - height, offset)
                  offset = Math.min(offsetTop, offset)
                  if (fixed) {
                    elm.css({
                      top: offset + 'px'
                    })
                  }
                }
              }
            }
          } else {
            if (scroll > top) {
              fixed = true
              css = {
                position: 'fixed',
                top: offset
              }
              css.width = elm.css('box-sizing') === 'border-box' ? elm.outerWidth() + 'px' : elm.width() + 'px'
              elm.css(css).addClass(stickyClass)
              if (manualSpacer == null) {
                elm.after(spacer)
                if (elFloat === 'left' || elFloat === 'right') {
                  spacer.append(elm)
                }
              }
              elm.trigger('sticky_kit:stick')
            }
          }
          if (fixed && enableBottoming) {
            if (willBottom == null) {
              willBottom = scroll + height + offset > parentHeight + parentTop
            }
            if (!bottomed && willBottom) {
              bottomed = true
              if (parent.css('position') === 'static') {
                parent.css({
                  position: 'relative'
                })
              }
              return elm.css({
                position: 'absolute',
                bottom: paddingBottom,
                top: 'auto'
              }).trigger('sticky_kit:bottom')
            }
          }
        }
        recalsAndTick = function() {
          recalc()
          return tick()
        }
        detach = function() {
          detached = true
          win.off('touchmove', tick)
          win.off('scroll', tick)
          win.off('resize', recalsAndTick)
          $(document.body).off('sticky_kit:recalc', recalsAndTick)
          elm.off('sticky_kit:detach', detach)
          elm.removeData('sticky_kit')
          elm.css({
            position: '',
            bottom: '',
            top: '',
            width: ''
          })
          parent.position('position', '')
          if (fixed) {
            if (manualSpacer == null) {
              if (elFloat === 'left' || elFloat === 'right') {
                elm.insertAfter(spacer)
              }
              spacer.remove()
            }
            return elm.removeClass(stickyClass)
          }
        }
        win.on('touchmove', tick)
        win.on('scroll', tick)
        win.on('resize', recalsAndTick)
        $(document.body).on('sticky_kit:recalc', recalsAndTick)
        elm.on('sticky_kit:detach', detach)
        return setTimeout(tick, 0)
      }
      for (i = 0, len = this.length; i < len; i++) {
        elm = this[i]
        fn($(elm))
      }
      return this
    }
    $('#fedu').stickInParent({offsetTop: 70})
  }).call(this)
}
