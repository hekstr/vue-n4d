var $ = require('jquery')
module.exports = function() {
  (function() {
    let localStorage = window.localStorage
    localStorage.setItem('ma-skin', 'n4d-red')
    var currentSkin = localStorage.getItem('ma-skin') ? localStorage.getItem('ma-skin') : 'n4d-blue'
    $('[data-current-skin]').attr('data-current-skin', currentSkin)
    $('body').on('click', '[data-skin]', function() {
      var skin = $(this).data('skin')
      localStorage.setItem('ma-skin', skin)
      $('[data-current-skin]').attr('data-current-skin', skin)
    })
  })()
}
