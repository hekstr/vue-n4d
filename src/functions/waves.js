import Waves from 'node-waves'
import 'node-waves/dist/waves.css'
module.exports = function() {
  (function() {
    Waves.attach('.btn:not(.btn-icon):not(.btn-float)')
    Waves.attach('.btn-icon, .btn-float', ['waves-circle', 'waves-float'])
    Waves.init()
  })()
}
