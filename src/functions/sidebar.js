var $ = require('jquery')
require('malihu-custom-scrollbar-plugin')($)
require('malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')
module.exports = function() {
  (function() {
    // Toggle
    $('.c-overflow').mCustomScrollbar({
      theme: 'minimal-dark',
      scrollInertia: 100,
      axis: 'yx',
      mouseWheel: {
        enable: true,
        axis: 'y',
        preventDefault: true
      }
    })
  })()
}
