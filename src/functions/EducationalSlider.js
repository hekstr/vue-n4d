var $ = require('jquery')

// function debounce(func, wait, immediate) {
//   var timeout
//   return function() {
//     var context = this
//     var args = arguments
//     var later = function() {
//       timeout = null
//       if (!immediate) func.apply(context, args)
//     }
//     var callNow = immediate && !timeout
//     clearTimeout(timeout)
//     timeout = setTimeout(later, wait)
//     if (callNow) func.apply(context, args)
//   }
// }
import Swiper from 'swiper'
import 'swiper/dist/css/swiper.min.css'
module.exports = function() {
  (function() {
    if ($('.Educational__slider').length > 0) {
      var educationalSwiper = null
      var mq = window.matchMedia('(max-width: 992px)')
      var EducationalSlider = function() {
        if (mq.matches) {
          EducationalSliderInit()
        }
      }

      $(window).on('debouncedresize', function(event) {
        if (educationalSwiper && !mq.matches) {
          educationalSwiper.destroy(true, true)
          educationalSwiper = undefined
        }
        if (!educationalSwiper && mq.matches) {
          EducationalSliderInit()
        }
      })
      var EducationalSliderInit = function() {
        console.log(666)
        educationalSwiper = new Swiper('.Educational__slider', {
          direction: 'horizontal',
          loop: true,
          wrapperClass: 'Educational__slider--wrapper',
          slideClass: 'Box__educational',
          pagination: '.swiper-pagination',
          paginationBulletRender: function(index, className) {
            return '<span data-current-skin class="' + className + '"></span>'
          },
          'preloadImages': false,
          'lazyLoading': true
        })
      }
      EducationalSlider()
    }
  })()
}
