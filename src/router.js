import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from './components/Home'
import Article from './components/Article'
import Category from './components/Category'
// import CategoryArticle from './components/categories/Article'
import EducationalsList from './components/educationals/List'
import Educational from './components/Educational'
import Tag from './components/Tag'
import Sponsor from './components/Sponsor'
import SearchList from './components/search/List'

const router = new VueRouter({
  history: true,
  root: 'edukacja'
})

router.map({
  '/': {
    name: 'home',
    component: Home
  },
  '/:articleSlug': {
    name: 'article',
    component: Article
  },
  '/kategoria/:slug': {
    name: 'category',
    component: Category
  },
  '/kategoria/:slug/:articleSlug': {
    name: 'categoryArticle',
    component: Article
  },
  '/tag/:slug': {
    name: 'tag',
    component: Tag
  },
  '/tag/:slug/:articleSlug': {
    name: 'tagArticle',
    component: Article
  },
  '/partner-edukacyjny/:slug': {
    name: 'sponsor',
    component: Sponsor
  },
  '/partner-edukacyjny/:slug/:articleSlug': {
    name: 'sponsorArticle',
    component: Article
  },
  '/programy-edukacyjne': {
    name: 'educationals',
    component: EducationalsList
  },
  '/programy-edukacyjne/:slug': {
    name: 'educational',
    component: Educational
  },
  '/programy-edukacyjne/:slug/:articleSlug': {
    name: 'educationalArticle',
    component: Article
  },
  '/wyszukaj/:querySlug': {
    name: 'search',
    component: SearchList
  }
})
  // For every new route scroll to the top of the page
router.beforeEach(function() {
  window.scrollTo(0, 0)
})
router.redirect({
  '*': '/'
})
export default router
