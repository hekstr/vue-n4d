// An action will receive the store as the first argument.
export const incrementCounter = makeAction('INCREMENT', 1)
export const toggleSidebar = makeAction('TOGGLE_SIDEBAR')
export const hideSidebar = makeAction('HIDE_SIDEBAR')
export const toggleSearchBox = makeAction('TOGGLE_SEARCH_BOX')
export const toggleFullScreenMode = makeAction('TOGGLE_FULL_SCREEN_MODE')
export const hideSearchBox = makeAction('HIDE_SEARCH_BOX')
export const setSearchQuery = makeAction('SET_SEARCH_QUERY', 1)
export const setBreadcrumbs = makeAction('SET_BREADCRUMBS', 1)
export const startLoader = makeAction('START_LOADER')
export const finishLoader = makeAction('FINISH_LOADER')
export const setN4DUser = makeAction('SET_N4D_USER', 1)
export const setSponsor = makeAction('SET_SPONSOR', 1)

function makeAction(type) {
  return ({
    dispatch
  }, ...args) => dispatch(type, ...args)
}
