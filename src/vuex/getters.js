// This getter is a function which just returns the count
// Without ES6 you can also write it as:
// export function getCount (state) {
//   return state.count
// }
export const getCount = state => state.count
export const getSidebarToggled = state => state.sidebarToggled
export const getSearchBoxToggled = state => state.searchBoxToggled
export const getApi = state => state.api
export const getFullScreenMode = state => state.fullScreenMode
export const getBreadcrumbs = state => state.breadcrumbs
export const getLoader = state => state.isLoader
export const getN4DUser = state => state.n4dUser
export const getSponsor = state => state.sponsor
export const getSearchQuery = state => state.searchQuery
