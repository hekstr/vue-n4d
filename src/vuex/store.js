import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  // api: 'http://n4d2.lo/api/v2/',
  api: 'http://n4d.pl/api/v2/',
  count: 0,
  sidebarToggled: false,
  searchBoxToggled: false,
  fullScreenMode: false,
  searchQuery: '',
  isLoader: false,
  breadcrumbs: [{
    name: 'home',
    title: 'Edukacja'
  }],
  n4dUser: {},
  sponsor: {}
}

const mutations = {
  TOGGLE_FULL_SCREEN_MODE(state) {
    state.fullScreenMode = !state.fullScreenMode
  },
  INCREMENT(state, amount) {
    state.count = state.count + amount
  },
  TOGGLE_SIDEBAR(state) {
    state.sidebarToggled = !state.sidebarToggled
  },
  HIDE_SIDEBAR(state) {
    state.sidebarToggled = false
  },
  TOGGLE_SEARCH_BOX(state) {
    state.searchBoxToggled = !state.searchBoxToggled
  },
  HIDE_SEARCH_BOX(state) {
    state.searchBoxToggled = false
  },
  SET_SEARCH_QUERY(state, query) {
    state.searchQuery = query
  },
  START_LOADER(state) {
    state.isLoader = true
  },
  FINISH_LOADER(state) {
    state.isLoader = false
  },
  SET_BREADCRUMBS(state, breadcrumbs) {
    state.breadcrumbs = breadcrumbs
  },
  SET_N4D_USER(state, n4dUser) {
    state.n4dUser = n4dUser
  },
  SET_SPONSOR(state, sponsor) {
    state.sponsor = sponsor
  }
}

export default new Vuex.Store({
  state,
  mutations
})
